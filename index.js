import express, { json } from "express";
// import { traineesRouter } from "./src/route/traineesRouter.js";
// import { firstRouter } from "./src/route/firstRouter.js";
// import { nameRouter } from "./src/route/nameRouter.js";
// import { bikeRouter } from "./src/route/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";
import studentRouter from "./src/myRoute/studentRoute.js";
import bookRoute from "./src/myRoute/bookRoute.js";
import classRoute from "./src/myRoute/classRoute.js";
import collegeRoute from "./src/myRoute/collegeRoute.js";
import departmentRoute from "./src/myRoute/departmentRoute.js";
import traineeRoute from "./src/myRoute/traineeRoute.js";

let expressApp = express();
expressApp.use(json());
connectToMongoDb();

// expressApp.use(
//   (req, res, next) => {
//     console.log("This is Normal Middleware 1 in Application");
//     let err = new Error("Application Error!!!!!!!!!");
//     next(err);
//   },
//   (req, res, next) => {
//     console.log("This is Normal Middleware 2 in Application");
//   },
//   (req, res, next) => {
//     console.log("This is Normal Middleware 2 in Application ");
//     next(err);
//   },
//   (err, req, res, next) => {
//     console.log(err.message);
//   }
// );

// expressApp.use("/trainees", traineesRouter);

// expressApp.use("/", firstRouter);
// expressApp.use("/names", nameRouter);
// expressApp.use("/bikes", bikeRouter);
// expressApp.use("/students", studentRouter)

expressApp.use("/books", bookRoute);
expressApp.use("/classes", classRoute);
expressApp.use("/colleges", collegeRoute);
expressApp.use("/departments", departmentRoute);
expressApp.use("/students", studentRouter);
expressApp.use("/trainees", traineeRoute);

expressApp.listen(8000, () => {
  console.log("app is listening at port 8000");
});
