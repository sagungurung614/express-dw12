import { Router } from "express";

let departmentRoute = Router();

departmentRoute.route("/").post((req, res, next) => {
  res.json("Trainee Post");
});

export default departmentRoute;
