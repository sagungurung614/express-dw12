import { Router } from "express";

let bookRoute = Router();

bookRoute.route("/").post((req, res, next) => {
  res.json("Book Post");
});

export default bookRoute;
