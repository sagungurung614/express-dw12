import { Router } from "express";

let classRoute = Router();

classRoute.route("/").post((req, res, next) => {
  res.json("Class Post");
});

export default classRoute;
