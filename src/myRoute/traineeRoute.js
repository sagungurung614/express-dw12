import { Router } from "express";

let traineeRoute = Router();

traineeRoute.route("/").post((req, res, next) => {
  res.json("Trainee Post");
});

export default traineeRoute;
