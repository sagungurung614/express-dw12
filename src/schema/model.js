/* define array = > moobjectdel
name, object
Student = [
    {name: " ", age:  , isMarried: true/false}, {}.....
] */

import bookSchema from "./bookSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./traineeSchema.js";
import collegeSchema from "./collegeSchema.js";
import classSchema from "./classroomSchema.js";
import departmentSchema from "./departmentSchema.js";
import { model } from "mongoose";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);

export let Trainee = model("Trainee", traineeSchema);
export let College = model("College", collegeSchema);
export let ClassRoom = model("Classroom", classSchema);
export let Department = model("Department", departmentSchema);

/* first letter of model name must be capital and singular
    Variable name and model name must be same */
