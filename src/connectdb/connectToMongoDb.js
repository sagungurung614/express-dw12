import mongoose from "mongoose";
import { port } from "../constant.js";

const connectToMongoDb = () => {
  mongoose.connect(port);
  console.log("Application is connected to MongoDb successfully");
};

export default connectToMongoDb;
